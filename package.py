name = "usd_lib"

version = "19.05.0"

authors = [
    "Deepak Mathur",
    "Brian Silva",
]

description = \
    """
    Pixar USD Libraries
    """

external = True

tools = []

requires = [
    'boost-1.61.0'
]

variants = [
    ['platform-windows', 'arch-AMD64', 'os-windows-10.0.16299', 'maya-2018.*'],
    ['platform-windows', 'arch-AMD64', 'os-windows-10.0.17763', 'maya-2019.*'],
]

uuid = "locksmith_pixar_usd_lib"

def commands():
    import os  # This must be imported here. See https://github.com/nerdvegas/rez/wiki/Package-Commands#overview
    import re

    usd_version = '{}.{}'.format(this.version.major, this.version.minor)
    usd_variant = os.path.normpath(this.root.replace(this.base, ''))

    # TODO: This is a horrible hack, but only necessary until we properly build USD through rez.
    # Remove Maya from variant path.
    # What we should REALLY do, is release USD and the USD Maya plug-ins as separate rez packages.
    usd_variant = re.sub('maya-.+', '', usd_variant)

    usd_install = os.path.normpath('{package_root}/USD/{version}/{variant}'.format(
        package_root=this.config.release_packages_path,
        version=usd_version,
        variant=usd_variant
    ))

    info('Using USD installation: {}'.format(usd_install))

    # Check that the usd_install directory is there.
    if not os.path.exists(usd_install):
        stop('A USD installation could not be found at {}. Please contact the pipeline team.'.format(usd_install))

    env.PYTHONPATH.append( os.path.join(usd_install, 'lib', 'python'))
    env.PATH.append(       os.path.join(usd_install, 'lib'))
    env.PATH.append(       os.path.join(usd_install, 'bin'))

    # Setup for the Maya plug-in if installed.
    if 'maya' in resolve:
        maya_plugin_dir = os.path.join('third_party', 'maya-{v}'.format(v=resolve.maya.version.major))
        env.PATH.append(              os.path.join(usd_install, maya_plugin_dir, os.path.normpath('lib')))
        env.MAYA_PLUG_IN_PATH.append( os.path.join(usd_install, maya_plugin_dir, os.path.normpath('plugin')))
        env.MAYA_SCRIPT_PATH.append(  os.path.join(usd_install, maya_plugin_dir, os.path.normpath('lib/usd/usdMaya/resources')))
        env.MAYA_SCRIPT_PATH.append(  os.path.join(usd_install, maya_plugin_dir, os.path.normpath('plugin/pxrUsdPreviewSurface/resources')))
        env.XBMLANGPATH.append(       os.path.join(usd_install, maya_plugin_dir, os.path.normpath('lib/usd/usdMaya/resources')))
